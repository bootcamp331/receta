const mongoose = require('mongoose')
const recipeSchema = require('./models/recipe')

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true,
        lowercase: true
    },
    schema: {
        type: Number,
        required: true
    },
    recipes: {
        type: [recipeSchema],
        validate: {
            validator: v => v.length > 0,
            message: "Sin recetas"
        }
    },
})

module.exports = mongoose.model('user', userSchema)